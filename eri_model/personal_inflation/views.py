import os
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import requests
import openpyxl
import json
from io import BytesIO
from eri_model.settings import BASE_DIR
from bs4 import BeautifulSoup

@csrf_exempt
def process_excel_data(request):
    if request.method == 'POST':
        url = "https://stat.gov.kz/ru/industries/economy/prices/spreadsheets/?year=&name=19117&period=&type="

        # Отправляем GET-запрос к странице
        response = requests.get(url)

        # Проверяем успешность запроса
        if response.status_code == 200:
            # Используем BeautifulSoup для парсинга HTML-кода страницы
            soup = BeautifulSoup(response.text, 'html.parser')
            
            # Находим все элементы с классом "divTableCell" и тегом "a"
            table_cells = soup.select('.divTableCell a')
            parse_url = table_cells[0].get('href')
            
            url = f'https://stat.gov.kz{parse_url}'
            try:
                response = requests.get(url)
                response.raise_for_status()  # Проверить статус запроса
                excel_buffer = BytesIO(response.content)
                workbook = openpyxl.load_workbook(excel_buffer)
                
                try:
                    with open(f'{BASE_DIR}/personal_inflation/data/data.json', 'r', encoding='utf-8') as json_file:
                        try:
                            data_to_extract = json.load(json_file)
                        except json.JSONDecodeError as e:
                            return JsonResponse({'error': f"Ошибка при загрузке JSON файла: {e}"}, status=500)

                    dict_res = {}
                    for category, items in data_to_extract.items():
                        
                        dict_item = {}  
                        if category == "Продовольственные товары":
                            sheet_name = '1.1'
                        elif category == "Непродовольственные товары":
                            sheet_name = '1.2'
                        else:
                            sheet_name = '1.3'
                        
                        for item in items:
                            if item in ["Продовольственные товары", "Непродовольственные товары", "Платные услуги"]:
                                sheet = workbook['1']
                            elif item in ["Табачные изделия", "Одежда", "Твердое топливо"]:
                                sheet = workbook['4']
                            else:
                                sheet = workbook[sheet_name]
                            for row_number, row_value in enumerate(sheet['A'], start=1):
                                if row_value.value == item:
                                    data_month = sheet.cell(row=row_number, column=2).value
                                    data_year = sheet.cell(row=row_number, column=6).value
                                    dict_item[item] = [data_month, data_year]
                                    break
                        
                        dict_res[category] = dict_item     

                    json_output_path = f'{BASE_DIR}/personal_inflation/data/personal_inflation_index.json'
                    with open(json_output_path, 'w', encoding='utf-8') as json_output:
                        json.dump(dict_res, json_output, ensure_ascii=False, indent=2)

                    return JsonResponse({'message': f'Результат сохранен'}, status=200)
                finally:
                    workbook.close()
            except requests.exceptions.RequestException as req_ex:
                return JsonResponse({'error': f"Ошибка при запросе к серверу: {req_ex}"}, status=500)
            except Exception as ex:
                return JsonResponse({'error': f"Возникла ошибка: {ex}"}, status=500)
        else:
            return JsonResponse({'status': 'error', 'message': 'Ошибка при запросе к https://stat.gov.kz/ru/industries/economy/prices/spreadsheets/?year=&name=19117&period=&type='}, status=405)
    else:
        return JsonResponse({'status': 'error', 'message': 'Invalid request method'}, status=405)

def get_json_data(request, file_path):
    if request.method == 'GET':
        if not os.path.isfile(f'{BASE_DIR}/personal_inflation/data/{file_path}.json'):
            return JsonResponse({'error': 'File not found'}, status=404)
        with open(f'{BASE_DIR}/personal_inflation/data/{file_path}.json', 'r', encoding='utf-8') as file:
            data = json.load(file)

        return JsonResponse(data)
    else:
        return JsonResponse({'status': 'error', 'message': 'Invalid request method'}, status=405)

def get_inflation_var1(request):
    if request.method == 'GET' and request.content_type == 'application/json':
        try:
            # Получение данных из тела запроса в формате JSON
            json_data = json.loads(request.body)
            print(json_data)
            
            with open(f'{BASE_DIR}/personal_inflation/data/personal_inflation_index.json', "r", encoding="utf-8") as file:
                index = json.load(file)

            with open(f'{BASE_DIR}/personal_inflation/data/weight.json', "r", encoding="utf-8") as file:
                weight = json.load(file)

            sum_weight = 0
            sum_1 = 0
            sum_2 = 0

            for parameter_key, parameter_value in json_data.items():
                for elem in parameter_value:
                    if elem in index[parameter_key].keys():
                        sum_weight += weight[parameter_key][elem]
                        sum_1 += index[parameter_key][elem][0] * weight[parameter_key][elem] 
                        sum_2 += index[parameter_key][elem][1] * weight[parameter_key][elem]
            print(sum_1, sum_weight)

            result_1 = round((sum_1 / sum_weight) - 100, 1)
            result_2 = round((sum_2 / sum_weight) - 100, 1)

            return JsonResponse({"inflation_var1_month": result_1, "inflation_var1_year": result_2})
        except json.JSONDecodeError:
            return JsonResponse({'status': 'error', 'message': 'Invalid JSON format'}, status=400)
    else:
        return JsonResponse({'status': 'error', 'message': 'Invalid request method or content type'}, status=405)
    
def get_inflation_var2(request):
    if request.method == 'GET' and request.content_type == 'application/json':
        try:
            # Получение данных из тела запроса в формате JSON
            json_data = json.loads(request.body)
            
            with open(f'{BASE_DIR}/personal_inflation/data/personal_inflation_index.json', "r", encoding="utf-8") as file:
                index = json.load(file)

            sum_value = 0
            sum_1 = 0
            sum_2 = 0

            for parameter_key, parameter_value in json_data.items():
                for parameter_value_key, parameter_value_value in parameter_value.items():
                    if parameter_value_key in index[parameter_key].keys():
                        sum_value += parameter_value_value
                        sum_1 += index[parameter_key][parameter_value_key][0] * parameter_value_value
                        sum_2 += index[parameter_key][parameter_value_key][1] * parameter_value_value

            result_month = round((sum_1 / sum_value) - 100, 1) if sum_value != 0 else 0
            result_year = round((sum_2  / sum_value) - 100, 1) if sum_value != 0 else 0
            print((round(sum_2  / sum_value, 1)))

            return JsonResponse({"inflation_var_month": result_month, "inflation_var_year": result_year})
        except json.JSONDecodeError:
            return JsonResponse({'status': 'error', 'message': 'Invalid JSON format'}, status=400)
    else:
        return JsonResponse({'status': 'error', 'message': 'Invalid request method or content type'}, status=405)



