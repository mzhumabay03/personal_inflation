from django.urls import path
from .views import *

urlpatterns = [
    path('process-excel-data/', process_excel_data, name='process_excel_data'),
    path('get-json-data/<str:file_path>/', get_json_data, name='get_json_data'),
    path('get-inflation-var1/', get_inflation_var1, name='get_inflation_var1'),
    path('get-inflation-var2/', get_inflation_var2, name='get_inflation_var2')
]
