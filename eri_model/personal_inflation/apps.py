from django.apps import AppConfig


class PersonalInflationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'personal_inflation'
